namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductConstructionStyleValue")]
    public partial class ProductConstructionStyleValue
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idConstructionStyle { get; set; }

        public int idConstructionField { get; set; }

        public int idProductDetail { get; set; }

        [Required]
        [StringLength(500)]
        public string chrValue { get; set; }

        public virtual ConstructionField ConstructionField { get; set; }

        public virtual ConstructionStyle ConstructionStyle { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
