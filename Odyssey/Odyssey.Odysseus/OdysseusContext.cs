namespace Odyssey.Odysseus
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class OdysseusContext : DbContext
    {
        public OdysseusContext()
            : base("name=OdysseusContext")
        {
        }

        public OdysseusContext(string tobase) : base(tobase) { }

        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<ConstructionField> ConstructionFields { get; set; }
        public virtual DbSet<ConstructionStyle> ConstructionStyles { get; set; }
        public virtual DbSet<ConstructionStyleField> ConstructionStyleFields { get; set; }
        public virtual DbSet<DimensionField> DimensionFields { get; set; }
        public virtual DbSet<DimensionProfile> DimensionProfiles { get; set; }
        public virtual DbSet<DimensionProfileDimensionField> DimensionProfileDimensionFields { get; set; }
        public virtual DbSet<DimensionProfileFieldDefault> DimensionProfileFieldDefaults { get; set; }
        public virtual DbSet<DimensionProfileValue> DimensionProfileValues { get; set; }
        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<ProductConstructionStyleValue> ProductConstructionStyleValues { get; set; }
        public virtual DbSet<ProductDetail> ProductDetails { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<Prop65> Prop65 { get; set; }
        public virtual DbSet<ProtectionType> ProtectionTypes { get; set; }
        public virtual DbSet<Size> Sizes { get; set; }
        public virtual DbSet<SKUDetail> SKUDetails { get; set; }
        public virtual DbSet<Test> Tests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //adding Blocks to filter out soft deletes
            modelBuilder.Entity<Brand>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<Test>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<Division>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ProductType>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<Category>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ProtectionType>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<Prop65>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ProductDetail>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ProductConstructionStyleValue>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ConstructionField>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ConstructionStyle>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<ConstructionStyleField>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<SKUDetail>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<Size>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<DimensionProfileValue>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<DimensionProfile>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<DimensionField>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<DimensionProfileDimensionField>().Map(m => m.Requires("bDeleted").HasValue(false));
            modelBuilder.Entity<DimensionProfileFieldDefault>().Map(m => m.Requires("bDeleted").HasValue(false));

            //basic binding
            modelBuilder.Entity<Brand>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<Brand>()
                .HasOptional(e => e.ProductDetail)
                .WithRequired(e => e.Brand);

            modelBuilder.Entity<Category>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<Category>()
                .HasMany(e => e.ProductDetails)
                .WithOptional(e => e.Category)
                .HasForeignKey(e => e.idCategory);

            modelBuilder.Entity<ConstructionField>()
                .Property(e => e.chrLabel)
                .IsFixedLength();

            modelBuilder.Entity<ConstructionField>()
                .HasMany(e => e.ConstructionStyleFields)
                .WithRequired(e => e.ConstructionField)
                .HasForeignKey(e => e.idConstructionField)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConstructionField>()
                .HasMany(e => e.ProductConstructionStyleValues)
                .WithRequired(e => e.ConstructionField)
                .HasForeignKey(e => e.idConstructionField)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConstructionStyle>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<ConstructionStyle>()
                .Property(e => e.txtDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ConstructionStyle>()
                .HasMany(e => e.ConstructionStyleFields)
                .WithRequired(e => e.ConstructionStyle)
                .HasForeignKey(e => e.idConstructionStyle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConstructionStyle>()
                .HasMany(e => e.ProductConstructionStyleValues)
                .WithRequired(e => e.ConstructionStyle)
                .HasForeignKey(e => e.idConstructionStyle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionField>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<DimensionField>()
                .Property(e => e.chrCode)
                .IsFixedLength();

            modelBuilder.Entity<DimensionField>()
                .Property(e => e.chrTolerance)
                .IsFixedLength();

            modelBuilder.Entity<DimensionField>()
                .HasMany(e => e.DimensionProfileDimensionFields)
                .WithRequired(e => e.DimensionField)
                .HasForeignKey(e => e.idDimensionField)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionField>()
                .HasMany(e => e.DimensionProfileFieldDefaults)
                .WithRequired(e => e.DimensionField)
                .HasForeignKey(e => e.idDimensionField)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionField>()
                .HasMany(e => e.DimensionProfileValues)
                .WithRequired(e => e.DimensionField)
                .HasForeignKey(e => e.idDimensionField)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionProfile>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<DimensionProfile>()
                .HasMany(e => e.DimensionProfileDimensionFields)
                .WithRequired(e => e.DimensionProfile)
                .HasForeignKey(e => e.idDimensionProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionProfile>()
                .HasMany(e => e.DimensionProfileFieldDefaults)
                .WithRequired(e => e.DimensionProfile)
                .HasForeignKey(e => e.idDimensionProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionProfile>()
                .HasMany(e => e.DimensionProfileValues)
                .WithRequired(e => e.DimensionProfile)
                .HasForeignKey(e => e.idDimensionProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DimensionProfileFieldDefault>()
                .Property(e => e.chrDefaultValue)
                .IsFixedLength();

            modelBuilder.Entity<DimensionProfileValue>()
                .Property(e => e.chrValue)
                .IsFixedLength();

            modelBuilder.Entity<Division>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<Division>()
                .HasMany(e => e.ProductDetails)
                .WithOptional(e => e.Division)
                .HasForeignKey(e => e.idDivision);

            modelBuilder.Entity<ProductConstructionStyleValue>()
                .Property(e => e.chrValue)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.BasePartNumber)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.txtDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.txtShortDescription)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.idProp65)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.idTesting)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .Property(e => e.idProtectionTypes)
                .IsFixedLength();

            modelBuilder.Entity<ProductDetail>()
                .HasMany(e => e.ProductConstructionStyleValues)
                .WithRequired(e => e.ProductDetail)
                .HasForeignKey(e => e.idProductDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductDetail>()
                .HasMany(e => e.SKUDetails)
                .WithRequired(e => e.ProductDetail)
                .HasForeignKey(e => e.idProductDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductType>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<ProductType>()
                .HasMany(e => e.ProductDetails)
                .WithOptional(e => e.ProductType)
                .HasForeignKey(e => e.idProductType);

            modelBuilder.Entity<Prop65>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<Prop65>()
                .Property(e => e.txtDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ProtectionType>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<ProtectionType>()
                .Property(e => e.chrDescription)
                .IsFixedLength();

            modelBuilder.Entity<Size>()
                .Property(e => e.chrName)
                .IsFixedLength();

            modelBuilder.Entity<Size>()
                .HasMany(e => e.DimensionProfileFieldDefaults)
                .WithRequired(e => e.Size)
                .HasForeignKey(e => e.idSize)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Size>()
                .HasMany(e => e.SKUDetails)
                .WithRequired(e => e.Size)
                .HasForeignKey(e => e.idSize)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrSKU)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrGoldenSampleLocation)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrUPC)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrInnerPackGTIN)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrMasterCaseGTIN)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrCT1GTIN)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .Property(e => e.chrPalletGTIN)
                .IsFixedLength();

            modelBuilder.Entity<SKUDetail>()
                .HasMany(e => e.DimensionProfileValues)
                .WithRequired(e => e.SKUDetail)
                .HasForeignKey(e => e.idSKUDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Test>()
                .Property(e => e.chrName)
                .IsFixedLength();
        }
    }
}
