namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConstructionField")]
    public partial class ConstructionField
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConstructionField()
        {
            ConstructionStyleFields = new HashSet<ConstructionStyleField>();
            ProductConstructionStyleValues = new HashSet<ProductConstructionStyleValue>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(30)]
        public string chrLabel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConstructionStyleField> ConstructionStyleFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductConstructionStyleValue> ProductConstructionStyleValues { get; set; }
    }
}
