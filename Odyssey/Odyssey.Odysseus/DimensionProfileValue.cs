namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DimensionProfileValue")]
    public partial class DimensionProfileValue
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idDimensionProfile { get; set; }

        public int idDimensionField { get; set; }

        [StringLength(20)]
        public string chrValue { get; set; }

        public int idSKUDetail { get; set; }

        public virtual DimensionField DimensionField { get; set; }

        public virtual DimensionProfile DimensionProfile { get; set; }

        public virtual SKUDetail SKUDetail { get; set; }
    }
}
