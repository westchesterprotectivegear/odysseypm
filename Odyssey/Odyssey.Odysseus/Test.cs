namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Test")]
    public partial class Test
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(100)]
        public string chrName { get; set; }
    }
}
