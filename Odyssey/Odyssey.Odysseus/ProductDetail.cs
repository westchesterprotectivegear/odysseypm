namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductDetail")]
    public partial class ProductDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductDetail()
        {
            ProductConstructionStyleValues = new HashSet<ProductConstructionStyleValue>();
            SKUDetails = new HashSet<SKUDetail>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(30)]
        public string BasePartNumber { get; set; }

        [Required]
        [StringLength(200)]
        public string chrName { get; set; }

        public byte bDeleted { get; set; }

        [Column(TypeName = "text")]
        public string txtDescription { get; set; }

        [StringLength(300)]
        public string txtShortDescription { get; set; }

        public int? idDivision { get; set; }

        public int? idProductType { get; set; }

        public int? idCategory { get; set; }

        public int? idBrand { get; set; }

        [StringLength(20)]
        public string idProp65 { get; set; }

        [StringLength(20)]
        public string idTesting { get; set; }

        [StringLength(20)]
        public string idProtectionTypes { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Category Category { get; set; }

        public virtual Division Division { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductConstructionStyleValue> ProductConstructionStyleValues { get; set; }

        public virtual ProductType ProductType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SKUDetail> SKUDetails { get; set; }
    }
}
