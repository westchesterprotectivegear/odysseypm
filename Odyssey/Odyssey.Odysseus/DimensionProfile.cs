namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DimensionProfile")]
    public partial class DimensionProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DimensionProfile()
        {
            DimensionProfileDimensionFields = new HashSet<DimensionProfileDimensionField>();
            DimensionProfileFieldDefaults = new HashSet<DimensionProfileFieldDefault>();
            DimensionProfileValues = new HashSet<DimensionProfileValue>();
        }

        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(30)]
        public string chrName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileDimensionField> DimensionProfileDimensionFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileFieldDefault> DimensionProfileFieldDefaults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileValue> DimensionProfileValues { get; set; }
    }
}
