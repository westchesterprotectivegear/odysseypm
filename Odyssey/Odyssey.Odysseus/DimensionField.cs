namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DimensionField")]
    public partial class DimensionField
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DimensionField()
        {
            DimensionProfileDimensionFields = new HashSet<DimensionProfileDimensionField>();
            DimensionProfileFieldDefaults = new HashSet<DimensionProfileFieldDefault>();
            DimensionProfileValues = new HashSet<DimensionProfileValue>();
        }

        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(30)]
        public string chrName { get; set; }

        [Required]
        [StringLength(5)]
        public string chrCode { get; set; }

        [Required]
        [StringLength(10)]
        public string chrTolerance { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileDimensionField> DimensionProfileDimensionFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileFieldDefault> DimensionProfileFieldDefaults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileValue> DimensionProfileValues { get; set; }
    }
}
