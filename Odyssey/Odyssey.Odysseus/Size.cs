namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Size")]
    public partial class Size
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Size()
        {
            DimensionProfileFieldDefaults = new HashSet<DimensionProfileFieldDefault>();
            SKUDetails = new HashSet<SKUDetail>();
        }

        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(20)]
        public string chrName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileFieldDefault> DimensionProfileFieldDefaults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SKUDetail> SKUDetails { get; set; }
    }
}
