namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConstructionStyleField")]
    public partial class ConstructionStyleField
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idConstructionStyle { get; set; }

        public int idConstructionField { get; set; }

        public virtual ConstructionField ConstructionField { get; set; }

        public virtual ConstructionStyle ConstructionStyle { get; set; }
    }
}
