namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Brand")]
    public partial class Brand
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(40)]
        public string chrName { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }
    }
}
