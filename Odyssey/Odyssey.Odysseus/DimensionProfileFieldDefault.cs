namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DimensionProfileFieldDefault")]
    public partial class DimensionProfileFieldDefault
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idDimensionProfile { get; set; }

        public int idDimensionField { get; set; }

        public int idSize { get; set; }

        [Required]
        [StringLength(20)]
        public string chrDefaultValue { get; set; }

        public virtual DimensionField DimensionField { get; set; }

        public virtual DimensionProfile DimensionProfile { get; set; }

        public virtual Size Size { get; set; }
    }
}
