namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SKUDetail")]
    public partial class SKUDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SKUDetail()
        {
            DimensionProfileValues = new HashSet<DimensionProfileValue>();
        }

        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idProductDetail { get; set; }

        public int idSize { get; set; }

        [Required]
        [StringLength(20)]
        public string chrSKU { get; set; }

        public int? idUnitOfMeasure { get; set; }

        public double? fltLength { get; set; }

        public double? fltWidth { get; set; }

        public double? fltHeight { get; set; }

        public double? fltWeight { get; set; }

        [StringLength(10)]
        public string chrGoldenSampleLocation { get; set; }

        public int? intUnitsPerPack { get; set; }

        public int? intUnitsPerCase { get; set; }

        public int? intUnitsPerCT1 { get; set; }

        public byte bSensormatic { get; set; }

        public byte bInnerOnTechPack { get; set; }

        public double? fltInnerLength { get; set; }

        public double? fltInnerWidth { get; set; }

        public double? fltInnerHeight { get; set; }

        public double? fltInnerVolume { get; set; }

        public double? fltInnerWeight { get; set; }

        [StringLength(20)]
        public string chrUPC { get; set; }

        [StringLength(20)]
        public string chrInnerPackGTIN { get; set; }

        [StringLength(20)]
        public string chrMasterCaseGTIN { get; set; }

        [StringLength(20)]
        public string chrCT1GTIN { get; set; }

        [StringLength(20)]
        public string chrPalletGTIN { get; set; }

        public double? fltCaseLength { get; set; }

        public double? fltCaseWidth { get; set; }

        public double? fltCaseHeight { get; set; }

        public double? fltCaseVolume { get; set; }

        public double? fltNetCaseWeight { get; set; }

        public double? fltGrossCaseWeight { get; set; }

        public double? fltCT1Length { get; set; }

        public double? fltCT1Width { get; set; }

        public double? fltCT1Height { get; set; }

        public double? fltCT1Volume { get; set; }

        public double? fltCT1GrossCaseWeight { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DimensionProfileValue> DimensionProfileValues { get; set; }

        public virtual ProductDetail ProductDetail { get; set; }

        public virtual Size Size { get; set; }
    }
}
