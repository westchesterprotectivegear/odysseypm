namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Prop65
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(20)]
        public string chrName { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string txtDescription { get; set; }
    }
}
