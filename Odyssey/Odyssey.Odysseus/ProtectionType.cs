namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProtectionType")]
    public partial class ProtectionType
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        [Required]
        [StringLength(20)]
        public string chrName { get; set; }

        [Required]
        [StringLength(200)]
        public string chrDescription { get; set; }
    }
}
