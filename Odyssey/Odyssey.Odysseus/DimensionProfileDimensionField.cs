namespace Odyssey.Odysseus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DimensionProfileDimensionField")]
    public partial class DimensionProfileDimensionField
    {
        public int ID { get; set; }

        public byte bDeleted { get; set; }

        public int idDimensionProfile { get; set; }

        public int idDimensionField { get; set; }

        public virtual DimensionField DimensionField { get; set; }

        public virtual DimensionProfile DimensionProfile { get; set; }
    }
}
