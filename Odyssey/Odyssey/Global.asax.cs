﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;

namespace Odyssey
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }
        public void RegisterRoutes(RouteCollection routes)
        {

            routes.MapPageRoute("Dimension", "dimensions/{sortCol}/{sortDir}", "~/Dimensions/Default.aspx");


            routes.MapPageRoute("Construction", "construction/{sortCol}/{sortDir}", "~/Construction/Default.aspx");


            routes.MapPageRoute("Products", "products/{sortCol}/{sortDir}", "~/Products/Default.aspx");


            routes.MapPageRoute("Default", "{sortCol}/{sortDir}", "~/Products/Default.aspx");
        }
    }
}