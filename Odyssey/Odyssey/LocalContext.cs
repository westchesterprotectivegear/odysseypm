﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Odyssey.Odysseus;

namespace Odyssey
{
    public class LocalContext : OdysseusContext
    {
            public LocalContext()
                : base("name=LocalContext")
            {
            }
        }
}