﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Odyssey.Construction
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Odyssey.LocalContext lc = new LocalContext();
            var con = lc.ConstructionStyles.Select(x=>x);

            ConstructionList.DataSource = con;
            ConstructionList.DataBind();
        }
    }
}